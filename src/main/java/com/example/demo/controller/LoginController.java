package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.form.UserForm;
import com.example.demo.model.User;

@Controller
public class LoginController {

	@GetMapping("/login")
	public String login(Model model) {
		UserForm userForm = new UserForm();
		model.addAttribute("userForm", userForm);

		return "login";
	}

	private List<User> initData() {
		List<User> users = new ArrayList<User>();
		users.add(new User("duong", "123456"));
		users.add(new User("huy", "123456"));
		return users;
	}

	@PostMapping("/login")
	public String login(Model model, @ModelAttribute("userForm") UserForm userForm) {
		
		User checkedUser = new User(userForm.getName().toString(), userForm.getPassword().toString());
		for (Iterator<User> iteratorUser = initData().iterator(); iteratorUser.hasNext();) {
			User user = (User) iteratorUser.next();
			System.out.println(user.toString());
			if (checkedUser.getName().equals(user.getName()) && (checkedUser.getPassword().equals(user.getPassword()))) {
				
				return "redirect:/index";
			}

		}
		
//		Iterator<User> iteratorUser = initData().iterator();
//		while (iteratorUser.hasNext()) {
//			User user = (User) iteratorUser.next();
//			System.out.println(user.toString());
//			
//			
//		}
//		System.out.println(checkedUser.getName().toString() + checkedUser.getPassword().toString());

//		if (checkedUser.getName().toString().equals("duong") && checkedUser.getPassword().toString().equals("123456")) {
//			return "redirect:/index";
//		}
		
		return "login";
		
	}
}
